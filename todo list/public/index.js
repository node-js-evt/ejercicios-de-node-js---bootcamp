let anado = document.querySelector('#btnAnadir');
anado.addEventListener('click', add);
let milista = document.querySelector('#contenido');
let nuevaTarea = document.querySelector('#tarea'); 

function pintar(){

while(milista.firstChild){
  milista.removeChild(milista.firstChild);
}

  fetch('http://localhost:3000/tasks', {method: 'get'}).then(function(respuesta) {
    // Convertir a JSON
    return respuesta.json();
  }).then(function(data) {

    for(let i in data) {
      let nuevo = document.createElement('li');
      nuevo.addEventListener('click', modificar);
      nuevo.textContent = data[i].title;
      nuevo.id = data[i]._id;
      let btn = document.createElement('button');
      btn.addEventListener('click', eliminar);
      btn.textContent = 'Eliminar';
      milista.appendChild(nuevo);
      nuevo.appendChild(btn);
    }
    
  });
}
pintar();

function add(){
  fetch('http://localhost:3000/tasks', {
    method: 'POST',
    headers:{
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body:JSON.stringify({
      title: nuevaTarea.value
    })
  }).then(response=>response.json())
    .then(data=>pintar())
    .catch(e=>console.error(e));

  location.reload(true);
};



function eliminar(e){
  console.log(e.target.parentNode.id);
  return fetch('http://localhost:3000/tasks' + '/' + e.target.parentNode.id, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json'
    }

  })
  .then(response => response.json())
  .then(data => {
    location.reload(true);
  })
  .catch(e => console.error(e));
};

function modificar(e){
  nuevaTarea.value = e.target.firstChild.textContent;
  anado.value = 'Modificar';
  anado.removeEventListener('click', add);
  anado.addEventListener('click', function(){
  fetch('http://localhost:3000/tasks'+ '/' + e.target.firstChild.parentNode.id, {
    method: 'put',
    headers:{
      'Content-Type': 'application/json',
    },
    body:JSON.stringify({
      title: nuevaTarea.value
    })
  }).then(response=>response.json())
    .catch(e=>console.error(e));

  location.reload(true);
  })
}