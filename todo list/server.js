const express = require('express'); //importo la librería express
const bodyParser = require('body-parser'); //esta librería es para poder acceder de forma sencilla a la respuesta del server
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/todo',  { useNewUrlParser: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console,'connection error:'));
db.once('open', function(){
    console.log("Estás conectado");
})

const Task = require('./model/TaskModel');



const app = express(); //express es un método y nuestra constante app será la que tenga acceso a sus atributos
app.use(express.static(__dirname + '/public')); //la carpeta public es la que servirá todo lo estático como HTML, CSS, imágenes...

app.use(bodyParser.json()); //la api acepta datos de json

app.get('/tasks', (request, response) => {

    Task.find(function (err, tareas){
        if(err){
            return response.status(500).send({error:'fallo al mostrar tareas'});
        }
        return response.json(tareas);
    })
});

app.post('/tasks', (request, response) => {
    const tarea = new Task(request.body);

    tarea.save((err, storedTask) =>{
        if (err){
            return response.status(500).send({error:'fallo al grabar tarea'})
        }
        return response.json(storedTask);
    });
});

app.put('/tasks/:id', (request, response) => { 
    let a = request.params.id;
    Task.findById(a, function (err, tareaACambiar) {
        if(err){
            return response.status(500).send({error:'fallo al modificar tarea'});
        }
        tareaACambiar.title = request.body.title;
        tareaACambiar.save((err, storedTask) =>{
            if (err){
                return response.status(500).send({error:'fallo'})
            }
            return response.json(storedTask);
        });
    });
});

app.delete('/tasks/:id', (request, response) => {
    let a = request.params.id;
    Task.deleteOne({ _id: a }, function (err) {
        if (err){
            return response.status(500).send({error:'fallo al borrar tarea'});
        }
        return response.json("Tarea borrada.");
      });
});


app.listen(3000)//lo pongo a escuchar en el puerto 3000