const express = require('express'); //importo la librería express
const bodyParser = require('body-parser'); //esta librería es para poder acceder de forma sencilla a la respuesta del server
const fakeDB = [];
const app = express(); //express es un método y nuestra constante app será la que tenga acceso a sus atributos
app.use(express.static(__dirname + '/public')); //la carpeta public es la que servirá todo lo estático como HTML, CSS, imágenes...
app.use(bodyParser.json()); //la api acepta datos de json
app.get('/', (request, response) => { //get, post, put y delete son los métodos más importantes de http
    response.send("Hola, clase!");
});
app.get('/movie', (request, response) => { //get, post, put y delete son los métodos más importantes de http
    response.send(fakeDB);
});
app.post('/movie', (request, response) => { //añade peli
    fakeDB.push(request.body);
    response.send(fakeDB);
});
app.get('/movie/:id', (request, response) => { //enseñar la peli cuyo id corresponda
    var id = request.params.id;
    response.send(fakeDB[id-1]);
});
app.put('/movie/:id', (request, response) => { //modificar la peli con ese id
    var id = request.params.id;
    fakeDB.splice(id-1, 1, request.body);
    response.send(fakeDB);
});
app.delete('/movie/:id', (request, response) => { //eliminar la peli con ese id
    var id = request.params.id;
    fakeDB.splice(id-1,1);
    response.send(fakeDB);
});
app.listen(3000)//lo pongo a escuchar en el puerto 3000