const express = require('express'); //importo la librería express
const bodyParser = require('body-parser'); //esta librería es para poder acceder de forma sencilla a la respuesta del server
const fakeDB = {
    '1':{
        Titulo:'Node',
        Director:'Dani',
        Año:2019
    },
    '2':{
        Titulo:'Terminator',
        Director:'James Cameron',
        Año:1984
    },
    '3':{
        Titulo:'Terminator II',
        Director:'James Cameron',
        Año:1985
    }
};
const app = express(); //express es un método y nuestra constante app será la que tenga acceso a sus atributos
app.use(express.static(__dirname + '/public')); //la carpeta public es la que servirá todo lo estático como HTML, CSS, imágenes...
app.use(bodyParser.json()); //la api acepta datos de json
app.get('/', (request, response) => { //get, post, put y delete son los métodos más importantes de http
    response.send("Hola, clase!");
});
app.get('/movie', (request, response) => { //get, post, put y delete son los métodos más importantes de http
    response.json(fakeDB);
});
app.post('/movie', (request, response) => { //añade peli
    const newID = new Date().getTime().toString();
    fakeDB[newID] = request.body;
    response.json(fakeDB[newID]);
});
app.get('/movie/:id', (request, response) => { //enseñar la peli cuyo id corresponda
    let mostrar = request.params.id;
    response.json(fakeDB[mostrar]);
});
app.put('/movie/:id', (request, response) => { //modificar la peli con ese id
    let modificar = request.params.id;
    const oldMoviveData = fakeDB[modificar];

    fakeDB[modificar] = { ...oldMoviveData, ...request.body  } ; //con los 3 puntos lo que hace es destruir en otro objeto, añadiendo los campos y sustituyendo los coincidentes
    response.json(fakeDB[modificar]);
});
app.delete('/movie/:id', (request, response) => { //eliminar la peli con ese id
    let borrar = request.params.id;
    delete fakeDB[borrar];
    response.json(fakeDB);
});
app.listen(3000)//lo pongo a escuchar en el puerto 3000